Shareboard, to synchronise your clipboard between multiple computers over the network.

Dependencies for client.py
==========================
  * python-xlib (library available in community)
  * pyxhook.py (on-the-fly library, included in the project)
  * xclip (binary available in extra)
  * xsel (binary available in community)
  * urllib2
  * libnotify (allows you to get notified)

Install:
========
  - Don't mind the server/web part, just care about client.py
  - Check every dep is there
  - Copy clientconf.py.default and servconf.py.default as clientconf.py and servconf.py.

Usage :
=======

Server :
--------
  * Launch as a daemon:
        $ ./server.py &>/dev/null &
  * I want some logs!
        $ ./server.py

Client :
--------
  * Launch as a daemon:
        $ ./client.py &>/dev/null &
  * I want some logs!
        $ ./client.py
