#!/usr/bin/env python2.7
# shareboard (c)


from lib.network import CNetwork, Message, REQ_MARK, NOTIF_MARK, YT_MARK
from lib.klogging import CLogger
import threading
import commands
import re
import os
from clientconf import *


def logging(msg):
    print(msg)


class Client(object):
    def __init__(self, host, port, chosen_keys):
        self.event = threading.Event()
        self.network = CNetwork(host, port, self.event)
        self.logger = CLogger(self.event, chosen_keys)

    def post_notification(self, msg, sender):
        command = 'notify-send -t 2500 -u critical'
        os.system(command + ' "' + sender + ' sent" "' + msg + '"')

    def unformat_notification(self, notif):
        i = re.search(r'' + NOTIF_MARK + '(?P<msg>[^' + NOTIF_MARK + ']+)' +
                      NOTIF_MARK + '(?P<sender>.*)', notif)
        return i.group('msg'), i.group('sender')

    def open_firefox(self, addr):
        os.system('firefox -new-tab ' + addr)
        logging('opened firefox with ' + addr)

    def open_mplayer(self, addr):
        os.system('stream ' + addr + ' 1>/dev/null 2>&1 &')
        logging('opened mplayer with ' + addr)

    def to_clipboard(self, addr):
        os.system('echo "' + addr + '" | xclip')

    def from_clipboard(self):
        print("From clipboard")
        return commands.getoutput('xsel')

    def kill(self):
        self.logger.logger.cancel()
        self.network.handle_close()
        sys.exit(0)

    def run(self):
        self.network.start()
        self.logger.start()

        while True:
            try:
                while True:
                    if self.event.wait(120):
                        break
            except KeyboardInterrupt:
                self.kill()

            if self.network.iswaiting:
                self.network.iswaiting = False
                inc = Message(self.network.in_buffer)

                if inc.type == 'notify':
                    self.post_notification(inc.content, inc.sender)
                    logging(inc.sender + ' copied ' + inc.content)
                elif inc.type == 'paste':
                    if re.search(RE_YOUTUBE, inc.content) is None or \
                            OPENING != 'mplayer':
                        self.open_firefox(inc.content)
                    elif OPENING == 'mplayer':
                        self.open_mplayer(inc.content)
                    else:
                        print('Bad settings: chose \'mplayer\' or \'firefox\'')
                        sys.exit(1)

                    self.to_clipboard(inc.content)
                    logging('you pasted ' + inc.content)

                self.network.in_buffer = ''
            elif self.logger.iswaiting:
                self.logger.iswaiting = False

                if self.logger.send:
                    inc = self.from_clipboard()

                    if len(inc) == 0:
                        logging('you tried sending an empty string')
                        continue

                    self.network.send_to_server(inc)
                    logging('you copied ' + inc)
                elif self.logger.get:
                    self.network.send_to_server(REQ_MARK)
                    logging('you asked for a paste')
                elif self.logger.grade:
                    self.network.send_to_server(
                        YT_MARK + self.logger.number + YT_MARK +
                        self.from_clipboard())
                    logging('sent ' + self.logger.number + ' to grade ' +
                            self.from_clipboard())

                self.logger.send = self.logger.get = self.logger.grade = False
            else:
                break

            self.event.clear()

Client(DISTANT_HOST, DISTANT_PORT, KEYS).run()
