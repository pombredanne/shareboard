#!/usr/bin/env python2.7
# shareboard (c)


from lib.network import SNetwork, Message, NOTIF_MARK
import datetime
import threading
import socket
import urllib2
import urllib
import sqlite3
import string
import random
import re
from servconf import *


def logging(msg):
    print(msg)


def create_ainv_table(file):
    conn = sqlite3.connect(file)

    conn.execute('CREATE TABLE IF NOT EXISTS ainv (name text, link text, \
            manny int, marc int, disco int, lysias int, date datetime)')

    conn.commit()
    conn.close()


def create_shareboard_tables(file):
    conn = sqlite3.connect(file)

    conn.execute('CREATE TABLE IF NOT EXISTS links (link text, \
            sender varchar(255), date datetime)')

    conn.execute('CREATE TABLE IF NOT EXISTS pastes (link text, content text, \
            sender varchar(255), date datetime)')

    conn.commit()
    conn.close()


def get_client_name(ips, ip):
    for current in ips.keys():
        if ips[current] == ip:
            return current


def is_website(addr):
    return addr[:4] == 'http'


def str_random_gen(num_letters=6):
    output = ''

    for x in range(0, num_letters):
        output += random.choice(string.ascii_uppercase + string.digits)

    return output


def fix_newlines(data):
    return string.replace(data, '\n', '<br/>\n')


def is_youtube(link):
    sh = re.search(r'http://youtu\.be/.{11}$', link)
    return not sh is None


def get_page_name(link, cutvalue):
    try:
        html = urllib2.urlopen(link).read().decode('utf-8')
    except urllib2.HTTPError:
        return link

    i = re.search(r'<title>(?P<boobs>[^<]+)</title>', html)

    if i is None:
        return link
    else:
        if is_youtube(link):
            output = i.group('boobs')[:-10]
        else:
            output = i.group('boobs')

        if len(output) > cutvalue:
            return output[:cutvalue] + '...'
        return output


def escape(html):
    return html.replace('&amp;', '&').replace('&lt;', '<').replace(
        '&gt;', '>').replace('&quot;', '"').replace("&#39;", "'")


class Server(object):
    def __init__(self, host, port, auth):
        self.event = threading.Event()
        self.allowed_clients = auth

        ip_list = []
        for client in auth.keys():
            ip_list.append(auth[client])
        self.network = SNetwork(host, port, self.event, ip_list)

        self.clip = 'www.google.com'
        self.clip_sender = 'server'

    def format_notification(self, msg, sender):
        return NOTIF_MARK + msg + NOTIF_MARK + sender

    def notify(self):
        tmp = get_client_name(self.allowed_clients, self.clip_sender)
        msg = self.format_notification(self.clip, tmp)

        for client in self.network.clients:
            if client[0] != self.clip_sender:
                self.network.send_to_client(msg, client[0])

    def setup_html(self, data):
        rand = str_random_gen()
        now = datetime.datetime.now()
        name = get_client_name(self.allowed_clients, self.clip_sender)

        conn = sqlite3.connect(SHAREBOARD_DATABASE)
        conn.execute('INSERT INTO pastes VALUES(?, ?, ?, ?)',
                     (BASE_URL + rand, str(data), name, now))

        conn.commit()
        conn.close()

        return BASE_URL + rand

    def save(self, data, sender):
        conn = sqlite3.connect(SHAREBOARD_DATABASE)
        name = get_client_name(self.allowed_clients, sender)
        now = datetime.datetime.now()
        conn.execute('INSERT INTO links VALUES(?, ?, ?)', (data, name, now))
        conn.commit()
        conn.close()

    def grade_youtube(self, sender, link, grade):
        if not is_youtube(link):
            return False

        authorized = ['manny', 'marc', 'disco', 'lysias']

        vidname = get_page_name(link, 60)

        name = get_client_name(self.allowed_clients, sender)

        if re.search(r'manny', name) is not None:
            name = 'manny'

        if re.search(r'null', name) is not None:
            name = 'marc'

        if not name in authorized:
            return False

        now = datetime.datetime.now()
        conn = sqlite3.connect(AINV_DATABASE)
        c = conn.cursor()
        c.execute('SELECT * FROM ainv WHERE link = ?', (link,))

        if c.fetchone():
            conn.execute('UPDATE ainv SET ' + name + ' = ? WHERE link = ?',
                         (int(grade), link))
        else:
            conn.execute('INSERT INTO ainv VALUES(?, ?, ?, ?, ?, ?, ?)',
                         (escape(vidname), link, -1, -1, -1, -1, now))
            conn.execute('UPDATE ainv SET ' + name + ' = ? WHERE link = ?',
                         (int(grade), link))

        conn.commit()
        conn.close()
        return True

    def send_MM(self, clip, sender):
        name = get_client_name(self.allowed_clients, sender)

        pagename = escape(urllib.unquote(get_page_name(clip, 210)))

        if re.search(r'manny', name) is not None:
            name = 'manny'

        if re.search(r'null', name) is not None:
            name = 'null'

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(('10.0.0.4', 5051))
        try:
            s.sendall('SB: ' + pagename.encode('utf-8') + ' :: ' + clip +
                      ' :: ' + name)
        except:
            s.sendall('SB: ' + clip + ' by ' + name)
        s.close()

    def MM_display_note(self, sender, note, clip):
        name = get_client_name(self.allowed_clients, sender)

        pagename = escape(urllib.unquote(get_page_name(clip, 210)))

        if re.search(r'manny', name) is not None:
            name = 'manny'

        if re.search(r'null', name) is not None:
            name = 'null'

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(('10.0.0.4', 5051))
        s.sendall('SB: <--- ' + pagename.encode('utf-8') + ' :: ' +
                  str(note) + ' :: ' + name)
        s.close()

    def run(self):
        self.network.start()

        while True:
            try:
                while True:
                    if self.event.wait(120):
                        break
            except KeyboardInterrupt:
                self.network.handle_close()
                break

            for client in self.network.clients:
                if client[1].iswaiting:
                    client[1].iswaiting = False
                    inc = Message(client[1].in_buffer)
                    na = get_client_name(self.allowed_clients, client[0])

                    if inc.type == 'request':
                        self.network.send_to_client(self.clip, client[0])
                        logging(client[0] + ' (' + na + ') ' + 'wants a paste')
                    elif inc.type == 'ainv':
                        if inc.content is None or inc.grade is None:
                            client[1].in_bufffer = ''
                            self.event.clear()
                            continue

                        if self.grade_youtube(client[0], inc.content,
                                              inc.grade):
                            logging(client[0] + ' (' + na + ') ' + 'graded ' +
                                    inc.content)
                            self.MM_display_note(client[0], inc.grade,
                                                 inc.content)
                        else:
                            print('grading error (sby not allowed/not yt ' +
                                  'link)')
                    elif inc.type == 'paste':
                        if inc.content == self.clip:
                            client[1].in_buffer = ''
                            self.event.clear()
                            continue

                        if is_website(inc.content):
                            self.clip = inc.content
                            self.clip_sender = client[0]
                        else:
                            self.clip_sender = client[0]
                            self.clip = self.setup_html(inc.content)

                        self.send_MM(self.clip, self.clip_sender)
                        logging(client[0] + ' (' + na + ') sent ' + self.clip)

                        self.save(self.clip, self.clip_sender)
                        self.notify()

                    client[1].in_buffer = ''
                    break

            self.event.clear()

create_ainv_table('ainv.db')
create_shareboard_tables('shareboard.db')
Server(HOST, PORT, ALLOWED_CLIENTS).run()
