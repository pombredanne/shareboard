#!/usr/bin/env python2.7
# shareboard (c)
#todo
#loop timeout
#exception handling on socket conn
#in_buffer emptying

import threading
import asyncore
import socket
import time
import re
import sys


END_MARK = chr(4)
NOTIF_MARK = chr(3)
REQ_MARK = chr(2)
YT_MARK = chr(8)
MSG_SIZE = 1024


def logging(msg):
    print(msg)


class SClientHandler(asyncore.dispatcher_with_send):
    def __init__(self, sock, snetwork):
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.event = snetwork.event
        self.out_buffer = ''
        self.in_buffer = ''
        self.iswaiting = False

        self.container = snetwork

    def handle_close(self):
        selfremoved = [cli for cli in self.container.clients if cli[1] != self]
        self.container.clients = selfremoved
        self.close()

    def handle_read(self):
        data = self.recv(MSG_SIZE)
        self.process(data)

    def process(self, data):
        if data == '':
            return
        elif data[-1] != END_MARK:
            self.in_buffer += data
            return

        self.in_buffer += data[:-1]

        self.iswaiting = True
        while self.event.is_set():
            time.sleep(0.1)
        self.event.set()


class SNetwork(threading.Thread, asyncore.dispatcher):
    def __init__(self, host, port, event, auth):
        threading.Thread.__init__(self)
        self.event = event

        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.bind((host, port))
        self.listen(5)

        self.allowed_clients = auth
        self.clients = []

    def handle_accept(self):
        pair = self.accept()

        if pair is None:
            pass
        else:
            sock, addr = pair

            if addr[0] in self.allowed_clients:
                logging(addr[0] + ' just connected')
                self.clients.append((addr[0], SClientHandler(sock, self)))
            else:
                logging(addr[0] + ' failed to connect')

    def send_to_client(self, data, client=None):
        data += END_MARK

        if client is None:
            for cli in self.clients:
                cli[1].out_buffer = data
        else:
            # We select the client with this address, there is only one
            cli = [cli for cli in self.clients if cli[0] == client][0]

            if cli is None:
                return

            cli[1].out_buffer = data

    def handle_close(self):
        self.close()

    def run(self):
        asyncore.loop(0.1)


class CNetwork(threading.Thread, asyncore.dispatcher_with_send):
    def __init__(self, host, port, event):
        asyncore.dispatcher_with_send.__init__(self)
        threading.Thread.__init__(self)
        self.event = event
        self.lock = threading.Lock()
        self.iswaiting = False

        asyncore.dispatcher.__init__(self)

        try:
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connect((host, port))
        except socket.error:
            print('could not connect to server')
            self.handle_close()

        self.out_buffer = ''
        self.in_buffer = ''
        self.host = host
        self.port = port

    def handle_close(self):
        print('lost connexion to server')
        self.close()
        sys.exit(2)

    def handle_error(self):
        print('something went wrong')
        sys.exit(3)

    def handle_read(self):
        data = self.recv(MSG_SIZE)
        self.process(data)

    def process(self, data):
        if data == '':
            return
        elif data[-1] != END_MARK:
            self.in_buffer += data
            return

        self.in_buffer += data[:-1]

        self.iswaiting = True
        while self.event.is_set():
            time.sleep(0.1)
        self.event.set()

    def send_to_server(self, data):
        self.out_buffer = data + END_MARK

    def run(self):
        asyncore.loop(0.1)


class Message:
    def __init__(self, inc_msg):
        if inc_msg[0] == REQ_MARK:
            self.type = 'request'
        elif inc_msg[0] == NOTIF_MARK:
            self.type = 'notify'

            i = re.search(r'(?P<msg>[^' + NOTIF_MARK + ']+)' + NOTIF_MARK +
                          '(?P<sender>.+)', inc_msg[1:])

            self.content = i.group('msg')
            self.sender = i.group('sender')
        elif inc_msg[0] == YT_MARK:
            self.type = 'ainv'
            i = re.search(r'(?P<grade>[^' + YT_MARK + ']+)' + YT_MARK +
                          '(?P<link>.+)', inc_msg[1:])

            if i is None:
                self.grade = self.content = None
            else:
                self.grade = int(i.group('grade'))
                self.content = i.group('link')
        else:
            self.type = 'paste'
            self.content = inc_msg
