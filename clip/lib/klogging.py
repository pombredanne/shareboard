#!/usr/bin/env python2.7
# shareboard (c)
#todo
#enable exit logger.start() loop


import threading
import pyxhook
import time


class CLogger(threading.Thread):
    def __init__(self, event, chosen_keys):
        threading.Thread.__init__(self)
        self.event = event
        self.iswaiting = False

        self.logger = pyxhook.HookManager()
        self.logger.HookKeyboard()
        self.logger.KeyDown = self.handle_down
        self.logger.KeyUp = self.handle_up

        self.keys = [False, False, False, False]
        self.send = self.get = self.grade = False
        self.chosen_keys = chosen_keys

        self.chosen_nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        self.number = ''
        self.number_pushed = 0

    def handle_down(self, event):
        m = self.keys[0] and self.keys[1]

        for i in range(0, 4):
            if event.Key in self.chosen_keys[i] or not self.chosen_keys[i]:
                self.keys[i] = True

                self.act()
                return
            elif m and event.Key in self.chosen_nums and \
                    self.number_pushed < 2:
                self.number_pushed += 1
                self.number += event.Key

                self.act()
                return

    def handle_up(self, event):
        for i in range(0, 4):
            if event.Key in self.chosen_keys[i] and self.chosen_keys[i]:
                self.keys[i] = False
                self.number = ''
                self.number_pushed = 0

    def act(self):
        if self.keys[0] and self.keys[1]:
            if self.keys[2]:
                self.send = True
            elif self.keys[3]:
                self.get = True
            elif self.number_pushed == 2:
                self.grade = True
            else:
                return
        else:
            return

        self.iswaiting = True
        while self.event.is_set():
            time.sleep(0.1)
        self.event.set()

    def run(self):
        self.logger.start()
